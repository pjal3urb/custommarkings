local mod_name = "CustomMarkings"
local localization_file = ModPath .. "localization/menu.json"
local settings_file = ModPath .. "settings.json"

CustomMarkings = CustomMarkings or class(MenuModBase)

CustomMarkings.ALL_UNIT_TYPES = { "tank", "tank_medic", "tank_mini", "spooc", "phalanx_vip", "phalanx", "medic", "shield", "sniper", "taser", "civilian" }
CustomMarkings.UNIT_GROUPS = {
	civilian_female = "civilian",
	bank_manager = "civilian",
}
CustomMarkings.MARK_TYPES = { "mark_enemy", "mark_enemy_damage_bonus", "mark_enemy_damage_bonus_distance" }

function CustomMarkings:init(...)
	CustomMarkings.super.init(self, ...)
	
	self:reset_active_selection()
end

function CustomMarkings:set_active_unit(unit_type) self._active_unit = unit_type end
function CustomMarkings:active_unit() return self._active_unit end
function CustomMarkings:set_active_mark(mark_type) self._active_mark = mark_type end
function CustomMarkings:active_mark() return self._active_mark end

function CustomMarkings:reset_active_selection()
	self._active_unit = CustomMarkings.ALL_UNIT_TYPES[1]
	self._active_mark = CustomMarkings.MARK_TYPES[1]
end

function CustomMarkings:menu_nodes()
	return { { id = "main", initiator = "CustomMarkingsInitiator", gui_class = "CustomMarkingsGuiNode" } }
end

function CustomMarkings:setup_menu(nodes)
	local function save(node, item)
		self:save()
	end
	
	local function value_changed(mod, item, value)
		mod:set_value(item.hierarchy, item.setting_name, value)
		mod:update_gui()
	end
	
	local function active_mark_changed(mod, item, value)
		mod:set_active_mark(value)
		MenuCallbackHandler:refresh_node()
	end
	
	local function active_unit_changed(mod, item, value)
		mod:set_active_unit(value)
		MenuCallbackHandler:refresh_node()
	end
	
	local function reset(mod, item, ...)
		mod:clear_settings(item.hierarchy)
		MenuCallbackHandler:refresh_node()
	end
	
	self:_setup_defaults()
	
	local unit_options = {}
	for _, utype in ipairs(CustomMarkings.ALL_UNIT_TYPES) do
		table.insert(unit_options, { value = utype })
	end
	
	local mark_options = {}
	for _, mtype in ipairs(CustomMarkings.MARK_TYPES) do
		table.insert(mark_options, { value = mtype })
	end
	
	local menus = {
		main = {
			back_callback = save,
			hierarchy = { self:active_unit(), self:active_mark() },
			
			{ "active_unit", "multichoice", { clbks = active_unit_changed, options = unit_options}},
			{ "div", "divider", { size = 12 }},
			{ "enabled", "toggle", { hierarchy = { self:active_unit() }, clbks = value_changed }},
			{ "div", "divider", { size = 4 }},
			{ "active_mark", "multichoice", { options = mark_options, clbks = active_mark_changed }},
			{ "color_space", "divider", { size = 48 }},
			{ "red", "slider", { min = 0, max = 1, step = 0.05, clbks = value_changed }},
			{ "green", "slider", { min = 0, max = 1, step = 0.05, clbks = value_changed }},
			{ "blue", "slider", { min = 0, max = 1, step = 0.05, clbks = value_changed }},
			{ "flash", "slider", { min = 0, max = 10, step = 0.1, clbks = value_changed }},
			{ "div", "divider", { size = 12 }},
			{ "reset", "button", { clbks = reset }},
		},
	}
	
	MenuBuilder.initialize_menu(nodes, self, menus, "main")
end

function CustomMarkings:_setup_defaults()
	local base_colors = {
		mark_enemy = tweak_data.contour.character.dangerous_color,
		mark_enemy_damage_bonus = tweak_data.contour.character.more_dangerous_color,
		mark_enemy_damage_bonus_distance = tweak_data.contour.character.more_dangerous_color,
	}
	
	--Initialize default values to match the standard colors from the original contours
	for _, utype in ipairs(CustomMarkings.ALL_UNIT_TYPES) do
		for _, mtype in ipairs(CustomMarkings.MARK_TYPES) do
			local c = base_colors[mtype]
			self:set_default_value({ utype, mtype }, "red", c.x)
			self:set_default_value({ utype, mtype }, "green", c.y)
			self:set_default_value({ utype, mtype }, "blue", c.z)
			self:set_default_value({ utype, mtype }, "flash", 0)
		end
		
		self:set_default_value({ utype }, "enabled", false)
	end
end

function CustomMarkings:update_gui()
	managers.menu:active_menu().renderer:active_node_gui():update_preview()
end

function CustomMarkings:get_custom_color_data(u_type, type)
	local unit = CustomMarkings.UNIT_GROUPS[u_type] or u_type

	if self:get_value({ unit }, "enabled") and table.contains(CustomMarkings.MARK_TYPES, type) then
		return {
			r = self:get_value({ unit, type }, "red"),
			g = self:get_value({ unit, type }, "green"),
			b = self:get_value({ unit, type }, "blue"),
			flash = self:get_value({ unit, type }, "flash")
		}
	end
end


CustomMarkingsInitiator = CustomMarkingsInitiator or class(MenuBuilderInitiator)

function CustomMarkingsInitiator:modify(node)
	self._mod:reset_active_selection()
	return CustomMarkingsInitiator.super.modify(self, node)
end

function CustomMarkingsInitiator:refresh(node)
	for _, item in ipairs(node:items()) do
		if item.hierarchy then
			if item.setting_name == "enabled" then
				item.hierarchy = { self._mod:active_unit() }
			else
				item.hierarchy = { self._mod:active_unit(), self._mod:active_mark() }
			end
		end
	end
	
	return CustomMarkingsInitiator.super.refresh(self, node)
end


MenuModRepository.register_mod(mod_name, settings_file, localization_file, CustomMarkings)
