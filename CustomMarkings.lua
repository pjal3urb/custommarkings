for _, mark in ipairs(CustomMarkings.MARK_TYPES) do
	ContourExt._types[mark].default_color = ContourExt._types[mark].default_color or ContourExt._types[mark].color
	ContourExt._types[mark].color = nil
end


local mod = MenuModRepository.mod("CustomMarkings")
if not mod:post_require(RequiredScript) then return end

local add_original = ContourExt.add

function ContourExt:add(type, ...)
	local setup = add_original(self, type, ...)
	local custom_data = mod:get_custom_color_data(self._unit:base()._tweak_table, type)
	
	if custom_data then
		setup.color = Vector3(custom_data.r, custom_data.g, custom_data.b)
		
		if custom_data.flash > 0 then
			self:flash(type, 1 / custom_data.flash)
		end
	elseif not self._types[type].color then
		setup.color = self._types[type].default_color
	end
	
	self:_apply_top_preset()
	
	return setup
end
