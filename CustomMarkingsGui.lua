CustomMarkingsGuiNode = CustomMarkingsGuiNode or class(MenuNodeGui)

function CustomMarkingsGuiNode:init(...)
	CustomMarkingsGuiNode.super.init(self, ...)
	
	local row_item = self:row_item_by_setting_name("color_space")
	local div = row_item.gui_panel
	local x, y, w, h = div:shape()
	
	self._mod = MenuModRepository.mod("CustomMarkings")
	self._preview_panel = self.item_panel:panel({ w = w, h = h, x = x, y = y })
	self._preview = self._preview_panel:rect({ h = h * 0.75, y = h * 0.125 })
	
	self:update_preview()
end

function CustomMarkingsGuiNode:refresh_gui(node)
	CustomMarkingsGuiNode.super.refresh_gui(self, node)
	self:update_preview()
end

function CustomMarkingsGuiNode:row_item_by_setting_name(setting_name)
	for _, row_item in ipairs(self.row_items) do
		if row_item.item.setting_name == setting_name then
			return row_item
		end
	end
end

function CustomMarkingsGuiNode:update_preview()
	local hierarchy = { self._mod:active_unit(), self._mod:active_mark() }
	
	self._preview:set_color(Color(
		self._mod:get_value(hierarchy, "red", 0),
		self._mod:get_value(hierarchy, "green", 0),
		self._mod:get_value(hierarchy, "blue", 0)
	))
	
	local old = self._flash_frequency or 0
	self._flash_frequency = self._mod:get_value(hierarchy, "flash", 0)
	self._flash_interval = 1 / self._flash_frequency
	
	if old == 0 then
		if self._flash_frequency > 0 then
			self._preview_panel:animate(callback(self, self, "_animate_flash"))
		end
	elseif self._flash_frequency <= 0 then
		self._preview_panel:set_visible(true)
		self._preview_panel:stop()
	end
end

function CustomMarkingsGuiNode:_animate_flash(p)
	local t = 0
	
	while true do
		t = t + coroutine.yield()
		
		if t > self._flash_interval then
			t = t - self._flash_interval
			p:set_visible(not p:visible())
		end
	end
end
